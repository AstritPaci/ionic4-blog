export interface IUser {
  id: number;
  name: string;
  email: string;
  password: string;
  photo: string;
  street: string;
  city: string;
  phone: string;
  website: string;
  auth: string;
  role: string;
}
