export interface ScrollHideInterface {
  cssProperty: string;
  maxValue: number;
}
