export interface ITopic {
  id: number;
  photo: string;
  title: string;
  userId: number;
}
