export interface IPost {
  id: number;
  title: string;
  body: string;
  photo: string;
  topicId: number;
  userId: number;
  map: { lat: number; lng: number };
}
