import { Component, OnInit } from '@angular/core';

import {
  Events,
  Platform,
  NavController,
  ToastController
} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StorageService } from './providers/storage/storage.service';
import { IUser } from './models/user/user.interface';
import { DataService } from './providers/data/data.service';
import { FcmService } from './providers/fcm-service/fcm.service';
import { ModalController } from '@ionic/angular';
import { Firebase } from '@ionic-native/firebase/ngx';
import { MenuController } from '@ionic/angular';
import { UserComponent } from './pages/users/user/user.component';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  user: IUser = {} as IUser;

  appPages = [
    {
      title: 'Topic',
      url: '/app/tabs/topics',
      icon: 'md-sunny'
    },
    {
      title: 'Posts',
      url: '/app/tabs/posts',
      icon: 'paper'
    },
    {
      title: 'Profile',
      url: '/app/tabs/profile',
      icon: 'md-person'
    },
    {
      title: 'Users',
      url: '/users',
      icon: 'md-contacts'
    },
    {
      title: 'Map',
      url: '/app/tabs/posts/map',
      icon: 'md-globe'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _storage: StorageService,
    public events: Events,
    private _data: DataService,
    private fcmService: FcmService,
    private navCtrl: NavController,
    private firebase: Firebase,
    private toastController: ToastController,
    public modalController: ModalController,
    private menuCtrl: MenuController
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.isLogedIn();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  pushNotification() {
    this.fcmService.setDeviceId();
    this.firebase.onNotificationOpen().subscribe((msg: any) => {
      if (this.platform.is('ios')) {
        console.log(msg.aps.alert);
      } else {
        console.log(msg);
        if (msg.tap === true) {
          this.navCtrl.navigateForward([msg.landing_page, msg.id]);
        } else if (msg.tap === false) {
          this.presentToast(msg.body);
        }
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.hide();
      this.splashScreen.hide();
      if (this.platform.is('cordova')) {
        console.log('----------');
        this.pushNotification();
      }
    });
  }

  isLogedIn() {
    this._storage.isLoggedIn().then(
      data => {
        if (data === true) {
          this.getUserData();
          this.listenForLogin();
        } else {
          this.listenForLogin();
        }
      },
      error => console.error(error)
    );
  }

  getUserData() {
    this._storage.getUserStorage().then(
      data => {
        this.user = JSON.parse(data);
        this.usertoShow();
        this._storage.setLloggedUserId(this.user.id);
        this._storage.setLoggedUserRole(this.user.role);
        this.fcmService.saveDevice(this.user.id);
      },
      error => console.error(error)
    );
  }
  test() {
    console.log(this.user);
  }
  listenForLogin() {
    this.events.subscribe('user:logged', () => {
      this.getUserData();
    });
  }

  async userModal() {
    const modal = await this.modalController.create({
      component: UserComponent,
      componentProps: {
        user: this.user
      }
    });
    this.menuCtrl.close();
    return await modal.present();
  }

  onLogOut() {
    this.user = {} as IUser;
    this._data.logOut();
  }

  usertoShow() {
    if (this.user.street === 'null') {
      this.user.street = null;
    }
    if (this.user.city === 'null') {
      this.user.city = null;
    }
    if (this.user.phone === 'null') {
      this.user.phone = null;
    }
    if (this.user.website === 'null') {
      this.user.website = null;
    }
  }
}
