import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './providers/auth-guard/auth.guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginPageModule',
    canLoad: [AuthGuardService]
  },
  {
    path: 'signup',
    loadChildren: './pages/signup/signup.module#SignupPageModule'
  },
  { path: 'app', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  {
    path: 'users',
    loadChildren:
      './pages/users/users-list/users-list.module#UsersListPageModule'
  },
  {
    path: 'user-edit',
    loadChildren: './pages/users/user-edit/user-edit.module#UserEditPageModule'
  },
  {
    path: 'profile/:id',
    loadChildren: './pages/profile/profile/profile.module#ProfilePageModule'
  },
  { path: 'maps', loadChildren: './pages/maps/maps.module#MapsPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
