import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from '@angular/fire/firestore';
import { IUser } from 'src/app/models/user/user.interface';
import { StorageService } from '../storage/storage.service';
import { Firebase } from '@ionic-native/firebase/ngx';

@Injectable({
  providedIn: 'root'
})
export class FcmService {
  user: IUser = {} as IUser;
  // Notification object to send
  private notificationObject = {
    data: {
      title: '',
      body: '',
      landing_page: '',
      id: null
    },
    to: '',
    priority: 'high',
    restricted_package_name: ''
  };

  // Device token to store
  private device = {
    deviceId: '',
    userId: null
  };
  private;
  private devicesCollection: AngularFirestoreCollection<any>;

  constructor(
    private afs: AngularFirestore,
    private http: HttpClient,
    private firebase: Firebase,
    private platform: Platform,
    private _storage: StorageService
  ) {
    this.devicesCollection = this.afs.collection<any>('devices');
  }

  async setDeviceId() {
    if (this.platform.is('android')) {
      this.device.deviceId = await this.firebase.getToken();
    }

    if (this.platform.is('ios')) {
      await this.firebase.grantPermission();
      this.device.deviceId = await this.firebase.getToken();
    }
  }

  saveDevice(userId: number) {
    this.loggedUser();
    this.device.userId = userId;
    if (this.device.deviceId) {
      return this.devicesCollection.doc(this.device.deviceId).set(this.device);
    }
  }

  gettokens(userId: number) {
    const devicesRef = this.afs
      .collection('devices', ref => ref.where('userId', '==', userId))
      .valueChanges();
    return devicesRef;
  }

  loggedUser() {
    this._storage.getUserStorage().then(
      data => {
        this.user = JSON.parse(data);
      },
      error => console.error(error)
    );
  }

  commentNotification(id: number, userId: number) {
    this.notificationObject.data.title = 'New Comment';
    this.notificationObject.data.body = `${
      this.user.name
    } commented in your post!`;
    this.notificationObject.data.landing_page = 'app/tabs/posts/show';
    this.notificationObject.data.id = id;
    this.gettokens(userId).subscribe(
      res => {
        res.forEach((item: any) => {
          this.notificationObject.to = item.deviceId;
          this.postRequest(this.notificationObject).subscribe(
            data => {},
            error => console.error(error)
          );
        });
      },
      error => console.error(error)
    );
  }

  postNotification(id: number, userId: number) {
    this.notificationObject.data.title = 'New Post';
    this.notificationObject.data.body = `${
      this.user.name
    } added new post to your topic!`;
    this.notificationObject.data.landing_page = 'app/tabs/posts/show';
    this.notificationObject.data.id = id;
    this.gettokens(userId).subscribe(
      res => {
        res.forEach((item: any) => {
          this.notificationObject.to = item.deviceId;
          this.postRequest(this.notificationObject).subscribe(
            data => {},
            error => console.error(error)
          );
        });
      },
      error => console.error(error)
    );
  }

  postRequest(data: any) {
    const headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set(
        'Authorization',
        // tslint:disable-next-line: max-line-length
        'key=AAAAVRS7QDY:APA91bGAFDikMLrqwpZ7_NyjRRMHT1Gr2Ai9Btpr6ECOqI89jlQ_os4cn3X5aXwDtOyv_gnyxAeK01KhoBLzb5cG9L8XMZXkZ1Kdg9V1W8AZGB-ifiEx_2eh_CnvrC4rpAeGFmHt5Y2m'
      );
    return this.http.post('https://fcm.googleapis.com/fcm/send', data, {
      headers: headers
    });
  }
}
