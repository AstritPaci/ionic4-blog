import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ApiInterceptor implements HttpInterceptor {
  constructor(private navCtrl: NavController) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const clonedReq = this.addHeaders(request);
    return next.handle(clonedReq).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 || error.status === 404) {
          this.navCtrl.navigateRoot(['login']);
        }
        return throwError(error);
      })
    );
  }

  private addHeaders(request: HttpRequest<any>): HttpRequest<any> {
    let clone: HttpRequest<any>;
    let headers: HttpHeaders = request.headers;

    if (typeof headers.keys()['Accept'] === 'undefined') {
      headers = headers.set('Accept', 'application/json');
    }

    if (typeof headers.keys()['Content-type'] === 'undefined') {
      headers = headers.set('Content-type', 'application/json');

      clone = request.clone({
        headers: headers
      });
      return clone;
    }
  }
}
