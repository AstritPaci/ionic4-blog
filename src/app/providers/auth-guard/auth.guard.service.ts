import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { StorageService } from '../storage/storage.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanLoad {
  constructor(
    private _storageService: StorageService,
    private navCtrl: NavController
  ) {}

  canLoad() {
    return this._storageService.isLoggedIn().then(data => {
      if (data === true) {
        this.navCtrl.navigateRoot('app/tabs/posts');
        return false;
      } else {
        return true;
      }
    });
  }
}
