import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from '@ionic/angular';
import { IUser } from 'src/app/models/user/user.interface';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private loggedUserId: number;
  private loggedUserRole: string;
  constructor(private storage: Storage, public events: Events) {}

  setUserStorage(user: IUser) {
    this.storage.set('USER', JSON.stringify(user));
    this.loggedUserId = user.id;
    this.emitEvent();
  }

  emitEvent() {
    setTimeout(() => {
      return this.events.publish('user:logged');
    }, 300);
  }

  getUserStorage() {
    return this.storage.get('USER');
  }

  removeUser() {
    return this.storage.remove('USER');
  }

  isLoggedIn(): Promise<boolean> {
    return this.getUserStorage().then(data => {
      return data ? true : false;
    });
  }

  setLloggedUserId(id: number) {
    this.loggedUserId = id;
  }

  getLloggedUserId() {
    return this.loggedUserId;
  }

  setLoggedUserRole(role: string) {
    console.log(role);
    this.loggedUserRole = role;
  }

  getLoggedUserRole(): string {
    console.log(this.loggedUserRole);
    return this.loggedUserRole;
  }
}
