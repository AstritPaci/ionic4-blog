import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUri = 'http://192.168.0.123:3000/';
  constructor(private http: HttpClient) {}

  public getRequest(path: string) {
    return this.http.get(this.apiUri + path);
  }

  public postRequest(path: string, data: any) {
    return this.http.post(this.apiUri + path, data);
  }

  public putRequest(path: string, data: any) {
    return this.http.put(this.apiUri + path, data);
  }

  public deleteRequest(path: string) {
    return this.http.delete(this.apiUri + path);
  }
}
