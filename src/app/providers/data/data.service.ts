import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { FavoriteService } from '../favorites/favorite.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  favorites: number[] = [];

  constructor(
    private _storage: StorageService,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private _favoriteService: FavoriteService
  ) {
    this._favoriteService.getFavorites().then(
      data => {
        if (data) {
          this.favorites = JSON.parse(data);
        }
      },
      error => console.error(error)
    );
  }

  hasFavorite(postId: number) {
    if (this.favorites) {
      return this.favorites.indexOf(postId) > -1;
    }
    return false;
  }

  addFavourite(postId: number) {
    this.favorites.push(postId);
    this._favoriteService
      .setFavorites(this.favorites)
      .then(
        data => console.log(data),
        error => this.favorites.splice(postId, 1)
      );
  }

  removeFavorite(postId: number) {
    const index = this.favorites.indexOf(postId);
    if (index > -1) {
      this.favorites.splice(index, 1);
      this._favoriteService
        .setFavorites(this.favorites)
        .then(data => console.log(data));
    }
  }

  randomImage(height: number, width: number): string {
    const number = Math.floor(Math.random() * (+100 - +1)) + +1;
    return `https://picsum.photos/${height}/${width}/?image=${number}`;
  }

  logOut() {
    this._storage.removeUser().then(
      () => {
        this.navCtrl.navigateRoot('login');
        this.menuCtrl.enable(false);
      },
      error => console.error(error)
    );
  }
}
