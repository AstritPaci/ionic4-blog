import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  constructor(private storage: Storage) {}

  setFavorites(fav: any) {
    return this.storage.set('FAVORITES', JSON.stringify(fav));
  }

  getFavorites() {
    return this.storage.get('FAVORITES');
  }
}
