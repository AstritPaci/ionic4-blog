import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { IUser } from 'src/app/models/user/user.interface';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class SocialLoginService {
  constructor(private _api: ApiService, private storage: StorageService) {}

  private userModel(user: any): IUser {
    const account: IUser = {
      id: null,
      name: user.name,
      email: user.email ? user.email : user.screen_name,
      password: 'null',
      photo: user.picture
        ? user.picture.data.url
        : user.profile_image_url_https.replace('_normal', ''),
      street: 'null',
      city: 'null',
      phone: 'null',
      website: 'null',
      auth: user.id,
      role: 'User'
    };
    return account;
  }

  login(user: any) {
    const account = this.userModel(user);
    return new Promise((resolve, reject) => {
      this._api
        .getRequest(`users?name=${account.name}&auth=${account.auth}`)
        .subscribe(
          (res: Array<IUser>) => {
            if (res.length < 1) {
              resolve(this.createUser(user));
            }
            this.storage.setUserStorage(res[0]);
            resolve(res[0]);
          },
          error => reject(false)
        );
    });
  }

  private createUser(user: any) {
    const account = this.userModel(user);
    return new Promise((resolve, reject) => {
      this._api.postRequest('users', account).subscribe(
        (res: IUser) => {
          this.storage.setUserStorage(res);
          resolve(account);
        },
        error => reject(false)
      );
    });
  }
}
