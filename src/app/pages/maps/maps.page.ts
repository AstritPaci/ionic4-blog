import { Component, OnInit, Input } from '@angular/core';
import { Platform, LoadingController } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  Environment,
  MyLocation
} from '@ionic-native/google-maps';
import { Router } from '@angular/router';
@Component({
  selector: 'app-maps',
  templateUrl: './maps.page.html',
  styleUrls: ['./maps.page.scss']
})
export class MapsPage implements OnInit {
  post: any;
  public map: GoogleMap;

  private loader: HTMLIonLoadingElement;

  constructor(
    private platform: Platform,
    private router: Router,
    public loadingController: LoadingController
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.post = this.router.getCurrentNavigation().extras.state.post;
    }
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  public async loadMap() {
    this.loader = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Loading...'
    });
    await this.loader.present();
    this.map = await GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 47.36175,
          lng: 7.50811
        },
        zoom: 3,
        tilt: 30
      }
    });

    this.post ? this.postLocation() : this.goToMyLocation();
  }

  goToMyLocation() {
    this.loader.dismiss();
    this.map.clear().then();

    // Get the location of you
    this.map
      .getMyLocation()
      .then((location: MyLocation) => {
        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          duration: 4000
        });

        this.map.addMarkerSync({
          title: 'My Location',
          position: location.latLng,
          animation: GoogleMapsAnimation.BOUNCE
        });
      })
      .catch(err => {
        console.error(err.error_message);
      });
  }

  postLocation() {
    this.loader.dismiss();
    this.map.clear();
    // Move the map camera to the location with animation
    this.map.animateCamera({
      target: this.post.map,
      zoom: 17,
      duration: 5000
    });

    this.map.addMarkerSync({
      title: this.post.title,
      position: this.post.map,
      animation: GoogleMapsAnimation.BOUNCE
    });
  }
}
