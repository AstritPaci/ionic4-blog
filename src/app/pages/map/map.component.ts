import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  Environment,
  MyLocation
} from '@ionic-native/google-maps';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  public map: GoogleMap;
  @Input() post: any;

  constructor(
    public modalController: ModalController,
    private platform: Platform
  ) {}

  ngOnInit() {
    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

  public async loadMap() {
    this.map = await GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 47.36175,
          lng: 7.50811
        },
        zoom: 3,
        tilt: 30
      }
    });

    // this.post ? this.postLocation() : this.goToMyLocation();
  }

  goToMyLocation() {
    this.map.clear().then();

    // Get the location of you
    this.map
      .getMyLocation()
      .then((location: MyLocation) => {
        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          duration: 5000
        });

        this.map.addMarkerSync({
          title: 'My Location',
          snippet: 'This plugin is awesome!',
          position: location.latLng,
          animation: GoogleMapsAnimation.BOUNCE
        });
      })
      .catch(err => {
        console.error(err.error_message);
      });
  }

  postLocation() {
    this.map.clear();
    this.map
      .getMyLocation()
      .then((location: MyLocation) => {
        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: this.post.map,
          zoom: 17,
          duration: 5000
        });

        this.map.addMarkerSync({
          title: this.post.title,
          snippet: 'This plugin is awesome!',
          position: this.post.map,
          animation: GoogleMapsAnimation.BOUNCE
        });
      })
      .catch(err => {
        console.error(err.error_message);
      });
  }
}
