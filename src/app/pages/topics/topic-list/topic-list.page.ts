import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/providers/api/api.service';
import { NavController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.page.html',
  styleUrls: ['./topic-list.page.scss']
})
export class TopicListPage implements OnInit {
  topics: any;
  constructor(
    private _api: ApiService,
    private navCtrl: NavController,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this._api.getRequest('topics?_expand=user').subscribe(
      res => {
        this.topics = res;
      },
      error => console.error(error)
    );
  }

  onShow(topicId: any) {
    this.navCtrl.navigateForward(['app/tabs/topics/show', topicId]);
  }
}
