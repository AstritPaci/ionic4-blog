import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { TopicPopoverpage } from '../topic-popover';
import { ApiService } from 'src/app/providers/api/api.service';
import { LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/providers/storage/storage.service';

@Component({
  selector: 'app-topic-show',
  templateUrl: './topic-show.page.html',
  styleUrls: ['./topic-show.page.scss']
})
export class TopicShowPage implements OnInit {
  optionButton = false;
  topicId = null;
  topic: any;

  constructor(
    private popoverCtrl: PopoverController,
    private route: ActivatedRoute,
    private _api: ApiService,
    public loadingController: LoadingController,
    private _storage: StorageService
  ) {
    this.loadData();
  }

  async loadData() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Loading...'
    });
    await loading.present();
    this.topicId = this.route.snapshot.paramMap.get('id');
    this._api
      .getRequest(`topics/${this.topicId}?_expand=user&_embed=posts`)
      .subscribe(
        res => {
          this.topic = res;
          if (
            this.topic.userId === this._storage.getLloggedUserId() &&
            this._storage.getLoggedUserRole() === 'Admin'
          ) {
            this.optionButton = true;
          }
          loading.dismiss();
        },
        error => {
          console.error(error);
          loading.dismiss();
        }
      );
  }

  ngOnInit() {}

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: TopicPopoverpage,
      event,
      componentProps: {
        topic: this.topic
      }
    });
    await popover.present();
  }
}
