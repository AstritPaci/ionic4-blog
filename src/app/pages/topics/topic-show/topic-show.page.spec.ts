import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicShowPage } from './topic-show.page';

describe('TopicShowPage', () => {
  let component: TopicShowPage;
  let fixture: ComponentFixture<TopicShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicShowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
