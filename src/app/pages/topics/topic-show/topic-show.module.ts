import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TopicShowPage } from './topic-show.page';
import { TopicPopoverpage } from '../topic-popover';

const routes: Routes = [
  {
    path: '',
    component: TopicShowPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TopicShowPage, TopicPopoverpage],
  entryComponents: [TopicPopoverpage]
})
export class TopicShowPageModule {}
