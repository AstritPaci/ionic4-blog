import { Component, OnInit } from '@angular/core';

import {
  PopoverController,
  NavParams,
  AlertController,
  NavController
} from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { ApiService } from 'src/app/providers/api/api.service';

@Component({
  template: `
    <ion-list>
      <ion-item text-center button (click)="onEdit()">
        <ion-label>Edit</ion-label>
      </ion-item>
      <ion-item text-center button (click)="onDeleteAlert()">
        <ion-label color="danger">Delete</ion-label>
      </ion-item>
    </ion-list>
  `
})
export class TopicPopoverpage implements OnInit {
  topic: any;
  constructor(
    public popoverCtrl: PopoverController,
    private navParams: NavParams,
    private alertController: AlertController,
    private navCtrl: NavController,
    private router: Router,
    private _api: ApiService
  ) {}

  ngOnInit(): void {
    this.topic = this.navParams.get('topic');
  }

  onEdit() {
    const data: NavigationExtras = {
      state: {
        topic: this.topic
      }
    };
    this.navCtrl.navigateForward(['app/tabs/topics/add'], data);
    this.popoverCtrl.dismiss();
  }

  onDelete() {
    this._api
      .deleteRequest(`topics/${this.topic.id}`)
      .subscribe(
        () => this.navCtrl.navigateBack('app/tabs/topics'),
        error => console.error(error)
      );
  }

  async onDeleteAlert() {
    const alert = await this.alertController.create({
      header: 'Delete',
      message: 'Delete this topic!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          cssClass: 'alertDanger',
          handler: () => {
            this.onDelete();
          }
        }
      ]
    });
    this.popoverCtrl.dismiss();
    await alert.present();
  }
}
