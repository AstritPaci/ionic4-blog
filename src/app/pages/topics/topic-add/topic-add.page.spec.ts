import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicAddPage } from './topic-add.page';

describe('TopicAddPage', () => {
  let component: TopicAddPage;
  let fixture: ComponentFixture<TopicAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
