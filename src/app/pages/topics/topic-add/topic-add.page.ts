import { Component, OnInit } from '@angular/core';
import { ITopic } from 'src/app/models/topic/topic.interfaces';
import { ApiService } from 'src/app/providers/api/api.service';
import { DataService } from 'src/app/providers/data/data.service';
import { NgForm } from '@angular/forms';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { IUser } from 'src/app/models/user/user.interface';

@Component({
  selector: 'app-topic-add',
  templateUrl: './topic-add.page.html',
  styleUrls: ['./topic-add.page.scss']
})
export class TopicAddPage implements OnInit {
  topic: ITopic = {} as ITopic;
  data: any;
  submitted = false;
  photo: string;

  constructor(
    private _api: ApiService,
    private _data: DataService,
    private _storage: StorageService,
    private navCtrl: NavController,
    private router: Router
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.topic = this.router.getCurrentNavigation().extras.state.topic;
      this.photo = this.topic.photo;
    }
  }

  async ngOnInit() {
    this._storage.getUserStorage().then(data => {
      const user: IUser = JSON.parse(data);
      if (user.role !== 'Admin') {
        this.navCtrl.navigateRoot('app/tabs/profile');
      }
    });
  }

  onSubmit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.topic.id ? this.editTopic(this.topic) : this.createTopic();
    }
  }

  createTopic() {
    if (this.topic.photo) {
      this.topic.userId = this._storage.getLloggedUserId();
      this._api
        .postRequest('topics', this.topic)
        .subscribe(
          () => this.navCtrl.navigateBack('app/tabs/topics'),
          error => console.log(error)
        );
    }
  }

  editTopic(topic) {
    delete topic.user;
    this._api
      .putRequest(`topics/${this.topic.id}`, this.topic)
      .subscribe(
        () => this.navCtrl.navigateBack('app/tabs/topics'),
        error => console.log(error)
      );
  }

  selectImage() {
    this.topic.photo = this._data.randomImage(200, 200);
    this.photo = this.topic.photo;
  }

  removeImg() {
    this.photo = null;
  }
}
