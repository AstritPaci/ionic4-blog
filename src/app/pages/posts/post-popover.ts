import { Component, OnInit, Input } from '@angular/core';
import {
  PopoverController,
  AlertController,
  NavController
} from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { ApiService } from 'src/app/providers/api/api.service';
@Component({
  template: `
    <ion-list>
      <ion-item text-center button (click)="onEdit()">
        <ion-label>Edit</ion-label>
      </ion-item>
      <ion-item text-center button (click)="onDeleteAlert()">
        <ion-label color="danger">Delete</ion-label>
      </ion-item>
    </ion-list>
  `
})
export class PostPopoverpage implements OnInit {
  @Input() post: any;
  constructor(
    public popoverCtrl: PopoverController,
    private alertController: AlertController,
    private navCtrl: NavController,
    private _api: ApiService
  ) {}
  ngOnInit(): void {}

  onEdit() {
    const data: NavigationExtras = {
      state: {
        post: this.post
      }
    };
    this.navCtrl.navigateForward('app/tabs/posts/add', data);
    this.popoverCtrl.dismiss();
  }

  onDelete() {
    this._api
      .deleteRequest(`posts/${this.post.id}`)
      .subscribe(
        () => this.navCtrl.navigateBack('app/tabs/posts'),
        error => console.error(error)
      );
  }

  async onDeleteAlert() {
    const alert = await this.alertController.create({
      header: 'Delete',
      message: 'Delete this post!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          cssClass: 'alertDanger',
          handler: () => {
            this.onDelete();
          }
        }
      ]
    });
    this.popoverCtrl.dismiss();
    await alert.present();
  }
}
