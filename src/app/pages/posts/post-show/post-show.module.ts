import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PostShowPage } from './post-show.page';
import { CommentsModalComponent } from '../comments-modal/comments-modal.component';
import { PostPopoverpage } from '../post-popover';

const routes: Routes = [
  {
    path: '',
    component: PostShowPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PostShowPage, CommentsModalComponent, PostPopoverpage],
  entryComponents: [CommentsModalComponent, PostPopoverpage]
})
export class PostShowPageModule {}
