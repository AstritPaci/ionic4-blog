import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import {
  ModalController,
  PopoverController,
  NavController
} from '@ionic/angular';
import { CommentsModalComponent } from '../comments-modal/comments-modal.component';
import { PostPopoverpage } from '../post-popover';
import { DataService } from 'src/app/providers/data/data.service';
import { ApiService } from 'src/app/providers/api/api.service';
import { LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/providers/storage/storage.service';

@Component({
  selector: 'app-post-show',
  templateUrl: './post-show.page.html',
  styleUrls: ['./post-show.page.scss']
})
export class PostShowPage implements OnInit {
  optionsButton = false;
  postId = null;
  post: any;
  isFavorite = false;
  constructor(
    private route: ActivatedRoute,
    public modalController: ModalController,
    public popoverCtrl: PopoverController,
    private _data: DataService,
    private _api: ApiService,
    public loadingController: LoadingController,
    private _storage: StorageService,
    private navCtrl: NavController
  ) {}

  async ngOnInit() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Loading...'
    });
    await loading.present();
    this.postId = this.route.snapshot.paramMap.get('id');
    this._api
      .getRequest(
        `posts/${this.postId}?_expand=topic&_expand=user&_embed=comments`
      )
      .subscribe(
        res => {
          this.post = res;
          this.isFavorite = this._data.hasFavorite(this.post.id);
          this.post.likes = Math.floor(Math.random() * (+10 - +2)) + +2;
          if (this.post.userId === this._storage.getLloggedUserId()) {
            this.optionsButton = true;
          }
          loading.dismiss();
        },
        error => {
          console.error(error);
          loading.dismiss();
        }
      );
  }

  toggleFavorite() {
    if (this._data.hasFavorite(this.post.id)) {
      this._data.removeFavorite(this.post.id);
      this.isFavorite = false;
    } else {
      this._data.addFavourite(this.post.id);
      this.isFavorite = true;
    }
  }

  async commentsModal() {
    const modal = await this.modalController.create({
      component: CommentsModalComponent,
      componentProps: {
        id: this.post.id,
        userId: this.post.userId
      }
    });
    return await modal.present();
  }

  async mapModal() {
    const data: NavigationExtras = {
      state: {
        post: this.post
      }
    };
    this.navCtrl.navigateForward('app/tabs/posts/map', data);
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PostPopoverpage,
      event,
      componentProps: {
        post: this.post
      }
    });
    await popover.present();
  }
}
