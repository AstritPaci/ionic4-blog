import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostShowPage } from './post-show.page';

describe('PostShowPage', () => {
  let component: PostShowPage;
  let fixture: ComponentFixture<PostShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostShowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
