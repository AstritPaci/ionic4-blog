import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PostListPage } from './post-list.page';
import { CommonDirectiveModule } from 'src/app/directives/common.module';

const routes: Routes = [
  {
    path: '',
    component: PostListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CommonDirectiveModule
  ],
  declarations: [PostListPage]
})
export class PostListPageModule {}
