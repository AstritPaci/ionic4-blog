import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/providers/api/api.service';
import { NavController } from '@ionic/angular';
import { DataService } from 'src/app/providers/data/data.service';
import { ScrollHideInterface } from 'src/app/models/scroll/scroll.interface';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.page.html',
  styleUrls: ['./post-list.page.scss']
})
export class PostListPage implements OnInit {
  footerScrollConfig: ScrollHideInterface = {
    cssProperty: 'margin-bottom',
    maxValue: undefined
  };
  headerScrollConfig: ScrollHideInterface = {
    cssProperty: 'margin-top',
    maxValue: 54
  };
  querySerach: string;
  posts: any;
  segment = 'all';
  constructor(
    private _api: ApiService,
    private _data: DataService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.updatePosts();
  }

  getPosts() {
    this._api
      .getRequest('posts?_expand=topic&_expand=user&_embed=comments')
      .subscribe(
        res => {
          this.posts = res;
          this.randNumber();
        },
        error => console.error(error)
      );
  }

  updatePosts() {
    if (this.segment === 'all') {
      this.getPosts();
    } else if (this.segment === 'favorites') {
      this.posts = this.posts.filter(post => this._data.hasFavorite(post.id));
    }
  }

  onShow(postId: number) {
    this.navCtrl.navigateForward(['app/tabs/posts/show', postId]);
  }

  searchPosts() {
    this._api
      .getRequest(
        `posts?q=${
          this.querySerach
        }&?_expand=topic&_expand=user&_embed=comments`
      )
      .subscribe(
        res => {
          this.posts = res;
          this.randNumber();
        },
        error => console.error(error)
      );
  }

  randNumber() {
    this.posts.forEach(post => {
      post.likes = Math.floor(Math.random() * (+10 - +2)) + +2;
    });
  }
}
