import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/providers/api/api.service';
import { IComment } from 'src/app/models/comment/comment.interface';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { IUser } from 'src/app/models/user/user.interface';
import { FcmService } from 'src/app/providers/fcm-service/fcm.service';

@Component({
  templateUrl: './comments-modal.component.html',
  styleUrls: ['./comments-modal.component.scss']
})
export class CommentsModalComponent implements OnInit {
  @Input() id: number;
  @Input() userId: number;
  user: IUser = {} as IUser;
  comments: any = [];
  myComment: any;
  form: IComment = {} as IComment;
  loggedUserId: number;

  constructor(
    private _api: ApiService,
    public modalController: ModalController,
    private _storage: StorageService,
    private fmc: FcmService
  ) {}

  ngOnInit() {
    this._api
      .getRequest(`comments?postId=${this.id}&_expand=user`)
      .subscribe(res => (this.comments = res), error => console.error(error));
    this.getUserData();
    console.log(this.form);
    this.loggedUserId = this._storage.getLloggedUserId();
  }

  closeModal(ev) {
    this.modalController.dismiss();
  }

  onSend() {
    if (this.form.body) {
      this.form.postId = this.id;
      this.form.userId = this._storage.getLloggedUserId();
      this._api.postRequest('comments', this.form).subscribe(
        res => {
          this.form.body = null;
          this.myComment = res;
          this.myComment.user = this.user;
          this.comments
            ? this.comments.push(this.myComment)
            : (this.comments = this.myComment);
          this.sendNotification();
        },
        error => console.error(error)
      );
    }
  }

  onDelete(commentId: number, index: number) {
    this._api
      .deleteRequest(`comments/${commentId}`)
      .subscribe(
        () => this.comments.splice(index, 1),
        error => console.error(error)
      );
  }

  sendNotification() {
    if (this.userId !== this._storage.getLloggedUserId()) {
      this.fmc.commentNotification(this.id, this.userId);
    }
  }

  getUserData() {
    this._storage
      .getUserStorage()
      .then(
        data => (this.user = JSON.parse(data)),
        error => console.error(error)
      );
  }
}
