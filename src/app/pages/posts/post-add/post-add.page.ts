import { Component, OnInit } from '@angular/core';
import { IPost } from 'src/app/models/post/post.interface';
import { DataService } from 'src/app/providers/data/data.service';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/providers/api/api.service';
import { ITopic } from 'src/app/models/topic/topic.interfaces';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { NavController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { FcmService } from 'src/app/providers/fcm-service/fcm.service';
import { MyLocation, LocationService } from '@ionic-native/google-maps';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.page.html',
  styleUrls: ['./post-add.page.scss']
})
export class PostAddPage implements OnInit {
  post: IPost = {} as IPost;
  topics: Array<ITopic> = [];
  submitted = false;
  focus = false;
  photo: string;
  loggedUserId: number;

  constructor(
    private _data: DataService,
    private _api: ApiService,
    private _storage: StorageService,
    private navCtrl: NavController,
    private router: Router,
    private fcm: FcmService,
    private platform: Platform
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.post = this.router.getCurrentNavigation().extras.state.post;
      this.photo = this.post.photo;
    }
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      LocationService.getMyLocation().then(
        (location: MyLocation) => {
          this.post.map = location.latLng;
        },
        error => console.error(error.error_message)
      );
    });
  }

  onSubmit(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      this.post.id ? this.editPost(this.post) : this.createPost();
    }
  }

  createPost() {
    if (this.post.photo) {
      this.post.userId = this.loggedUserId;

      this._api.postRequest('posts', this.post).subscribe(
        (data: any) => {
          this.topics.forEach(topic => {
            if (
              topic.id === this.post.topicId &&
              topic.userId !== this.loggedUserId
            ) {
              this.fcm.postNotification(data.id, topic.userId);
            }
          });
          this.navCtrl.navigateBack('app/tabs/posts');
        },
        error => console.log(error)
      );
    }
  }

  editPost(post: any) {
    delete post.user;
    delete post.topic;
    delete post.user;
    delete post.comments;
    delete post.likes;

    this._api
      .putRequest(`posts/${this.post.id}`, this.post)
      .subscribe(
        () => this.navCtrl.navigateBack('app/tabs/posts'),
        error => console.log(error)
      );
  }

  ionViewWillEnter() {
    this._api
      .getRequest('topics?_expand=user')
      .subscribe(
        (res: Array<ITopic>) => (this.topics = res),
        error => console.error(error)
      );
    this.loggedUserId = this._storage.getLloggedUserId();
  }

  selectImage() {
    this.post.photo = this._data.randomImage(300, 200);
    this.photo = this.post.photo;
  }

  removeImg() {
    this.photo = null;
  }

  setFocus() {
    this.focus = !this.focus;
  }
}
