import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ILogin } from '../../models/login/login.interface';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/providers/api/api.service';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { TwitterConnect } from '@ionic-native/twitter-connect/ngx';
import { SocialLoginService } from 'src/app/providers/social-login/social-login.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  account = { email: 'jhon@gmail.com', password: 'secret' } as ILogin;
  submitted = false;
  error: string;

  constructor(
    public navCtrl: NavController,
    public router: Router,
    private _api: ApiService,
    private _storage: StorageService,
    public menuCtrl: MenuController,
    private fb: Facebook,
    private tw: TwitterConnect,
    private _socialLogin: SocialLoginService,
    public loadingController: LoadingController
  ) {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {}

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this._api
        .getRequest(
          `users?email=${this.account.email}&password=${this.account.password}`
        )
        .subscribe(
          (res: any) => {
            if (res.length < 1) {
              this.error = 'Wrong Credentials';
            } else {
              this._storage.setUserStorage(res[0]);
              this.menuCtrl.enable(true);
              this.navCtrl.navigateRoot('app/tabs/posts');
            }
          },
          error => console.error(error)
        );
    }
  }

  facebookLogin() {
    this.fb
      .login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.fb
          .api('me?fields=id,name,email,picture.width(200).height(200)', [])
          .then(
            profile => {
              this.socialLogin(profile);
            },
            error => {
              console.error(error);
            }
          );
      })
      .catch(e => console.error('Error logging into Facebook', e));
  }

  twitterLogin() {
    this.tw.login().then(
      res => {
        console.log(res);
        // Get user data
        // There is a bug which fires the success event in the error event.
        // The issue is reported in https://github.com/chroa/twitter-connect-plugin/issues/23
        this.tw.showUser().then(
          profile => {
            this.socialLogin(profile);
          },
          error => {
            console.error(error);
          }
        );
      },
      error => {
        console.error(error);
      }
    );
  }

  async socialLogin(profile) {
    const loader = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Loading...'
    });
    await loader.present();
    this._socialLogin.login(profile).then(
      data => {
        loader.dismiss();
        this.menuCtrl.enable(true);
        this.navCtrl.navigateRoot('app/tabs/posts');
      },
      error => {
        loader.dismiss();
        console.error(error);
      }
    );
  }

  signUp() {
    this.router.navigateByUrl('signup');
  }
}
