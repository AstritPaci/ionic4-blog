import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuardService } from 'src/app/providers/auth-guard/auth.guard.service';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'topics',
        children: [
          {
            path: '',
            loadChildren:
              '../topics/topic-list/topic-list.module#TopicListPageModule'
          },
          {
            path: 'add',
            loadChildren:
              '../topics/topic-add/topic-add.module#TopicAddPageModule'
          },
          {
            path: 'show/:id',
            loadChildren:
              '../topics/topic-show/topic-show.module#TopicShowPageModule'
          }
        ]
      },
      {
        path: 'posts',
        children: [
          {
            path: '',
            loadChildren:
              '../posts/post-list/post-list.module#PostListPageModule'
          },

          {
            path: 'add',
            loadChildren: '../posts/post-add/post-add.module#PostAddPageModule'
          },
          {
            path: 'show/:id',
            loadChildren:
              '../posts/post-show/post-show.module#PostShowPageModule'
          },
          {
            path: 'map',
            loadChildren: '../maps/maps.module#MapsPageModule'
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: '../profile/profile/profile.module#ProfilePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/app/tabs/posts',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule {}
