import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { IUser } from 'src/app/models/user/user.interface';
import { ApiService } from 'src/app/providers/api/api.service';
import { DataService } from 'src/app/providers/data/data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage implements OnInit {
  account = { role: 'User' } as IUser;
  picture: string;
  submitted = false;
  constructor(
    public navCtrl: NavController,
    private _api: ApiService,
    private _data: DataService
  ) {}

  ngOnInit() {}

  onRegister(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      this.account.auth = 'null';
      this._api
        .postRequest('users', this.account)
        .subscribe(
          () => this.navCtrl.navigateBack('login'),
          error => console.error(error)
        );
    }
  }

  selectImage() {
    this.account.photo = this._data.randomImage(200, 200);
  }
}
