import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { IUser } from 'src/app/models/user/user.interface';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() user: IUser;
  loggedUserId: number;
  constructor(
    public modalController: ModalController,
    private navCtrl: NavController,
    private _storage: StorageService
  ) {
    this.loggedUserId = this._storage.getLloggedUserId();
  }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss();
  }

  editProfile() {
    const data: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.modalController.dismiss();
    this.navCtrl.navigateForward('user-edit', data);
  }
}
