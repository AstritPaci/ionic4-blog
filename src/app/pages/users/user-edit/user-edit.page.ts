import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/models/user/user.interface';
import { Router } from '@angular/router';
import { DataService } from 'src/app/providers/data/data.service';
import { NgForm } from '@angular/forms';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { ApiService } from 'src/app/providers/api/api.service';
import { NavController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.page.html',
  styleUrls: ['./user-edit.page.scss']
})
export class UserEditPage implements OnInit {
  submitted = false;
  photo: string;
  hasImage = true;
  user: IUser = {} as IUser;
  constructor(
    private router: Router,
    private _data: DataService,
    private _storage: StorageService,
    private _api: ApiService,
    private navCtrl: NavController
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.user = this.router.getCurrentNavigation().extras.state.user;
      this.photo = this.user.photo;
    }
  }

  ngOnInit() {}

  removeImg() {
    this.photo = 'assets/img/profile-placeholder.png';
    this.hasImage = false;
  }

  selectImage() {
    if (this.hasImage === false) {
      this.user.photo = this._data.randomImage(200, 200);
      this.photo = this.user.photo;
      this.hasImage = true;
    }
  }

  onSubmit(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      this._api
        .putRequest(`users/${this.user.id}`, this.user)
        .subscribe(
          (res: IUser) => this.editStorage(res),
          error => console.error(error)
        );
    }
  }

  editStorage(user: IUser) {
    this._storage.removeUser().then(
      () => {
        this._storage.setUserStorage(user);
        this.navCtrl.navigateBack('app/tabs/profile');
      },
      error => console.error(error)
    );
  }
}
