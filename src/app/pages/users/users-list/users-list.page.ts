import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/providers/api/api.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss']
})
export class UsersListPage implements OnInit {
  users: any;

  constructor(private _api: ApiService, private navCtrl: NavController) {}

  ngOnInit() {
    this._api.getRequest('users?_embed=posts').subscribe(
      res => {
        this.users = res;
      },
      error => console.error(error)
    );
  }

  onShow(userId: number) {
    this.navCtrl.navigateForward(`/profile/${userId}`);
  }
}
