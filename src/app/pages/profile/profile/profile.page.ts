import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/models/user/user.interface';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { NavController } from '@ionic/angular';
import { ITopic } from 'src/app/models/topic/topic.interfaces';
import { ApiService } from 'src/app/providers/api/api.service';
import { IPost } from 'src/app/models/post/post.interface';
import { ModalController } from '@ionic/angular';
import { UserComponent } from '../../users/user/user.component';
import { NavigationExtras, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss']
})
export class ProfilePage implements OnInit {
  user: IUser = {} as IUser;
  hasTopic = true;
  hasPosts = true;
  topics: Array<ITopic> = [] as Array<ITopic>;
  posts: Array<IPost> = [] as Array<IPost>;
  segment = 'posts';
  editButton = false;
  userId: any;

  constructor(
    private _storage: StorageService,
    private navCtrl: NavController,
    private _api: ApiService,
    public modalController: ModalController,
    private route: ActivatedRoute
  ) {
    this.userId = this.route.snapshot.paramMap.get('id');
  }

  ionViewWillEnter() {
    this.userId ? this.getUser() : this.userStorage();
  }

  userStorage() {
    this._storage.getUserStorage().then(
      data => {
        this.user = JSON.parse(data);
        this.usertoShow();
        if (this.user.id === this._storage.getLloggedUserId()) {
          this.editButton = true;
        }
        this.updateSegment();
      },
      error => console.error(error)
    );
  }

  getUser() {
    this._api.getRequest(`users/${this.userId}`).subscribe(
      (res: IUser) => {
        this.user = res;
        this.usertoShow();
        if (this.user.id === this._storage.getLloggedUserId()) {
          this.editButton = true;
        }
        this.updateSegment();
      },
      error => console.error(error)
    );
  }

  ngOnInit() {}

  updateSegment() {
    if (this.segment === 'posts') {
      this.getPosts();
    } else if (this.segment === 'topics') {
      this.getTopics();
    }
  }

  getPosts() {
    this._api.getRequest(`posts?userId=${this.user.id}`).subscribe(
      (res: Array<IPost>) => {
        this.posts = res;
        res.length > 0 ? (this.hasPosts = true) : (this.hasPosts = false);
      },
      error => console.error(error)
    );
  }

  getTopics() {
    this._api.getRequest(`topics?userId=${this.user.id}`).subscribe(
      (res: Array<ITopic>) => {
        this.topics = res;
        res.length > 0 ? (this.hasTopic = true) : (this.hasTopic = false);
      },
      error => console.error(error)
    );
  }

  onShowTopic(topicId: number) {
    this.navCtrl.navigateForward(['app/tabs/topics/show', topicId]);
  }

  onShowPosts(postId: number) {
    this.navCtrl.navigateForward(['app/tabs/posts/show', postId]);
  }

  onEdit() {
    const data: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.navCtrl.navigateForward('user-edit', data);
  }

  async userModal() {
    const modal = await this.modalController.create({
      component: UserComponent,
      componentProps: {
        user: this.user
      }
    });
    return await modal.present();
  }

  usertoShow() {
    if (this.user.street === 'null') {
      this.user.street = null;
    }
    if (this.user.city === 'null') {
      this.user.city = null;
    }
    if (this.user.phone === 'null') {
      this.user.phone = null;
    }
    if (this.user.website === 'null') {
      this.user.website = null;
    }
  }
}
